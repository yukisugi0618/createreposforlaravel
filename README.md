# CreateReposForLaravel

Laravelアプリで`$ php artisan make:model `で作られた`app/`直下のModelファイルを参照して、リポジトリパターンに自動で再構成します。

1.　 以下のコマンドをLaravelアプリのルートディレクトリ(いつも`$ php artisan`を叩いているディレクトリ)で実行します。

` $ git clone git@bitbucket.org:yukisugi0618/createreposforlaravel.git && ./createreposforlaravel/main.sh `

これによって以下の処理が実行されます。

- `/app/`直下のModelファイルを、`/app/Models/`へ移動
- `/app/Logics/`に各モデルのディレクトリを生成し、中にそれぞれInterfaceファイルとRepositoryファイルを自動生成
- `/app/Providers/RepositoryServiceProvider.php`ファイルを自動生成
- デフォルトでソース内の記述が`App\User`となっている箇所を`App\Models\User`へ置換
- Modelごとの`Controllerファイル`が存在しない場合、自動生成
- 自動構成用のファイルの削除

2.　その後、説明文に従い` /config/app.php `の` 'providers' `の部分に

` App\Providers\RepositoryServiceProvider::class, `

を追記してください。お手数ですが、こちらだけ手動でお願いいたします。

【注意】当スクリプトはLaravelアプリのプロジェクトを生成して、artisanでModelファイルを一通り生成した時点での使用を想定しています。

実装済みの箇所への破壊的な上書き処理は回避したつもりですが、念の為、実行前にGitでCommitしておくなど、スクリプト使用前の状態に巻き戻せる状態にしておいてください。