<?php

namespace CreateRepos;

class CreateRepos
{
    protected $service_list;

    public function  __construct()
    {
        $this->service_list = "";
    }

    public function  Main()
    {
        // 作成するファイル名を複数配列に格納

        $this->CreateDirectory('./app/Models');
        $this->CreateDirectory('./app/Logics');

        $models = glob('./app/*php');
        if (!$models) {
            echo "./app/直下にModelファイルはありませんでした。\n";
        } else {
            echo "./app/直下にModelファイルは".count($models)."個ありました。./app/Model/への移動とLogicファイルの生成を実行します。\n";
            // ファイルが入った配列を1つずつループ処理
            foreach ($models as $model) {
                $model = basename($model, ".php");
                $logic_dir_path = './app/Logics/' . $model;
                $this->CreateDirectory($logic_dir_path);

                $model_file_path = './app/' . $model . '.php';
                $this->ReplaceTextfile("namespace App;", "namespace App\\Models;", $model_file_path);
                $this->RenameFile($model_file_path, './app/Models/' . $model . '.php');

                $this->SaveTextfile($logic_dir_path . "/" . $model . "RepositoryInterface.php", $this->ShowTextModelInterface($model));
                $this->SaveTextfile($logic_dir_path . "/" . $model . "Repository.php", $this->ShowTextModelRepo($model));
                $this->CreateController($model);
            }
            echo "Modelファイルの移動とLogicディレクトリ内のファイルの生成が完了しました。\n";
        }
        $this->AddMovedModelsToServiceList();
        $this->SaveTextfile("./app/Providers/RepositoryServiceProvider.php", $this->ShowTextRepositoryServiceProvider());
        $this->FixAppUser("./app/Http/Controllers/Auth/RegisterController.php");
        $this->FixAppUser("./config/auth.php");
        $this->FixAppUser("./database/factories/UserFactory.php");
        $this->RepositoryServiceProviderOnConfigExists();
    }

    public function AddMovedModelsToServiceList()
    {
        foreach (glob('./app/Models/*') as $model) {
            $model_initial_caps = basename($model, ".php");
            
            $this->service_list .= "\t\t\$this->app->bind(\n";
            $this->service_list .= "\t\t\t\\App\\Logics\\${model_initial_caps}\\${model_initial_caps}RepositoryInterface::class,\n";
            $this->service_list .= "\t\t\t\\App\\Logics\\${model_initial_caps}\\${model_initial_caps}Repository::class,\n";
            $this->service_list .= "\t\t);\n";

            $this->CreateController($model);
        }
        return;
    }
    public function ShowTextRepositoryServiceProvider()
    {

        $service_list_clone = $this->service_list;
        $sp_file = <<<SP
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
${service_list_clone}
    }

    public function boot()
    {
    }
}
SP;
        return $sp_file;
    }


    public function ShowTextModelRepo(string $model)
    {
        $model_initial_caps = $model;
        $model_all_smalls = strtolower($model);
        $repo_file = <<<REPO
<?php
namespace App\Logics\\${model_initial_caps};
use App\Models\\${model_initial_caps};
use Illuminate\Support\Facades\DB;

class ${model_initial_caps}Repository implements ${model_initial_caps}RepositoryInterface
{
    protected \$${model_all_smalls};

    /**
    * @param object \$${model_all_smalls}
    */
    public function __construct(${model_initial_caps} \$${model_all_smalls})
    {
        \$this->${model_all_smalls} = \$${model_all_smalls};
    }

    public function createNew${model_initial_caps}()
    {
        return;
}

    public function edit${model_initial_caps}()
    {
        return;
}

}
REPO;
        $this->service_list .= "\t\t\$this->app->bind(\n";
        $this->service_list .= "\t\t\t\\App\\Logics\\${model_initial_caps}\\${model_initial_caps}Interface::class,\n";
        return $repo_file;
    }

    public function ShowTextModelInterface(string $model)
    {
        $model_initial_caps = $model;
        $model_all_smalls = strtolower($model);
        $if_file = <<<INTERFACE
<?php

namespace App\Logics\\${model_initial_caps};
use App\Models\\${model_initial_caps};

interface ${model_initial_caps}RepositoryInterface
{
    /**
     * @var string \$${model_all_smalls}
     * @return object
     */
    public function createNew${model_initial_caps}();
    public function edit${model_initial_caps}();
}
INTERFACE;
        $this->service_list .= "\t\t\t\\App\\Logics\\${model_initial_caps}\\${model_initial_caps}Repository::class,\n";
        $this->service_list .= "\t\t);\n";

        return $if_file;
    }

    public function CreateDirectory(string $dir)
    {
        $result = false;
        if (!file_exists($dir)) {
            mkdir($dir, 0775);
            echo $dir . "を生成しました。\n";
            $result = true;
        } else {
            echo $dir . "はすでに存在します。\n";
        }
        return $result;
    }

    public function SaveTextfile(string $file_path, string $text)
    {
        if (!file_put_contents($file_path,  $text)) {
            echo $file_path . "の保存に失敗しました。\n";
        } else {
            echo $file_path . "の保存に成功しました。\n";
        }
        return;
    }

    public function RenameFile(string $before, string $after)
    {
        if (!rename($before, $after)) {
            echo $before . "を" . $after . "へ移動できませんでした。\n";
        } else {
            echo $before . "を" . $after . "へ移動しました。\n";
        }
        return;
    }

    public function ReplaceTextfile(string $before, string $after, string $textfile_path)
    {
        $text = file_get_contents($textfile_path);
        $this->SaveTextfile($textfile_path, str_replace($before, $after, $text));
        return;
    }

    public function FixAppUser(string $textfile_path)
    {
        $text = file_get_contents($textfile_path);
        if (strpos($text, "App\\User")!==false) {
            $this->ReplaceTextfile("App\\User", "App\\Models\\User",$textfile_path);
            echo $textfile_path."のApp\\UserをApp\\Models\\Userへ修正しました。";
        }
        return;
    }

    public function CreateController(string $model)
    {
        
        $model = basename($model, ".php");
        $model_initial_caps = $model;
        $model_all_smalls = strtolower($model);
        $textfile_path = "app/Http/Controllers/".$model_initial_caps."Controller.php";
        $controller = <<<CC
<?php

namespace App\Http\\Controllers;
        
use App\Models\\${model_initial_caps};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
        
use App\Logics\\${model_initial_caps}\\${model_initial_caps}RepositoryInterface;
        
class ${model_initial_caps}Controller extends Controller
{

    public function __construct( ${model_initial_caps}RepositoryInterface \$${model_all_smalls}_repository)
    {
        \$this->${model_all_smalls}_repository = \$${model_all_smalls}_repository;
    }
        
}
CC;
        if(!file_exists($textfile_path)){
            $this->SaveTextfile($textfile_path,$controller);
            echo $model_initial_caps."のControllerファイルを新規生成しました。";
        }else{
            echo $model_initial_caps."のControllerファイルは存在するので、新規生成しませんでした。";
        }
        return;
    }

    public function RepositoryServiceProviderOnConfigExists()
    {
        $result = true;
        $text = file_get_contents("./config/app.php");
        $class = "App\\Providers\\RepositoryServiceProvider::class";
        if (!strpos($text, $class)) {
            echo "\n【注意】/config/app.php の 'providers' の部分に\n\nApp\\Providers\\RepositoryServiceProvider::class,\n\n が無いので、追記をお願いします。\n";
            echo "また、app/Providers/内のRepositoryServiceProvider.phpのregisterの中で列記されたメソッドと\nAppServiceProvider.phpのregisterで列記されたメソッドが重複しないようにお願いします。\n";
            $result = false;
        }
        return $result;
    }
}
$main = new CreateRepos;
$main->Main();
